class Knight extends Piece{
    constructor(color, player) {
        super(color, player);
        this.view = '<i class="fas fa-chess-knight fa-2x" style="color: ' + this.color + '"></i>';
    }

    getMoves(position) {
        return [
            new Position(position.getX() - 1, position.getY() + 2),
            new Position(position.getX() + 1, position.getY() + 2),
            new Position(position.getX() - 1, position.getY() - 2),
            new Position(position.getX() + 1, position.getY() - 2),
            new Position(position.getX() - 2, position.getY() + 1),
            new Position(position.getX() + 2, position.getY() + 1),
            new Position(position.getX() - 2, position.getY() - 1),
            new Position(position.getX() + 2, position.getY() - 1)
        ]
    }
}