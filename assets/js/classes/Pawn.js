class Pawn extends Piece {
    constructor(color, player) {
        super(color, player);
        this.view = '<i class="fas fa-chess-pawn fa-2x" style="color: ' + this.color + '"></i>';
    }

    getMoves(position) {
        if(this.player == 1) {
            let positions = [];

            if(position.getX() == 0) {
                positions = [
                    new Position(position.getX(), position.getY() - 1, false),
                    new Position(position.getX() + 1, position.getY() - 1)
                ];
            }
            else if(position.getX() == 7) {
                positions = [
                    new Position(position.getX(), position.getY() - 1, false),
                    new Position(position.getX() - 1, position.getY() - 1)
                ];
            }
            else {
                positions = [
                    new Position(position.getX(), position.getY() - 1, false),
                    new Position(position.getX() - 1, position.getY() - 1),
                    new Position(position.getX() + 1, position.getY() - 1)
                ];
            }

            if(position.getY() == 6)
                positions.push(new Position(position.getX(), 4));

            return positions;
        }
        else {
            let positions = [];

            if(position.getX() == 0) {
                positions = [
                    new Position(position.getX(), position.getY() + 1),
                    new Position(position.getX() + 1, position.getY() + 1)
                ];
            }
            else if(position.getX() == 7) {
                positions = [
                    new Position(position.getX(), position.getY() + 1),
                    new Position(position.getX() - 1, position.getY() + 1)
                ];
            }
            else {
                positions = [
                    new Position(position.getX(), position.getY() + 1),
                    new Position(position.getX() - 1, position.getY() + 1),
                    new Position(position.getX() + 1, position.getY() + 1)
                ];
            }

            if(position.getY() == 1)
                positions.push(new Position(position.getX(), 3, false));

            return positions;
        }
    }
}