class Chessboard {
    squares = [];
    selectedSquare = null;

    playerOneColor = 'black';
    playerTwoColor = 'blue';

    constructor() {
        this.squares = Array.from(new Array(8), (row, i) =>             
             Array.from(new Array(8), (col, j) =>
                new Square(null, new Position(parseInt(j), parseInt(i)))
            )
        )

        if(sessionStorage.getItem('playerOne') != null && sessionStorage.getItem('playerTwo') != null) {
            this.playerOneColor = sessionStorage.getItem('playerOne');
            this.playerTwoColor = sessionStorage.getItem('playerTwo');
        }

        this.init();

        this.render();
        this.listenForEvents();
    }

    // Initialisation des pièces sur l'échiquier
    init() {
        this.initPawns(6, this.playerOneColor, 1);
        this.initPawns(1, this.playerTwoColor, 2);
        this.initBishops();
        this.initKings();
        this.initQueens();
        this.initKnights();
        this.initRooks();
    }

    initPawns(line, color, player) {
        for(let i in this.squares[line]) {
            this.squares[line][i].setPiece(new Pawn(color, player));
        }
    }

    initBishops() {
        this.squares[7][2].setPiece(new Bishop(this.playerOneColor, 1));
        this.squares[7][5].setPiece(new Bishop(this.playerOneColor, 1));

        this.squares[0][2].setPiece(new Bishop(this.playerTwoColor, 2));
        this.squares[0][5].setPiece(new Bishop(this.playerTwoColor, 2));
    }

    initKings() {
        this.squares[7][4].setPiece(new King(this.playerOneColor, 1));

        this.squares[0][4].setPiece(new King(this.playerTwoColor, 2));
    }

    initQueens() {
        this.squares[7][3].setPiece(new Queen(this.playerOneColor, 1));

        this.squares[0][3].setPiece(new Queen(this.playerTwoColor, 2));
    }

    initKnights() {
        this.squares[7][1].setPiece(new Knight(this.playerOneColor, 1));
        this.squares[7][6].setPiece(new Knight(this.playerOneColor, 1));

        this.squares[0][1].setPiece(new Knight(this.playerTwoColor, 2));
        this.squares[0][6].setPiece(new Knight(this.playerTwoColor, 2));
    }

    initRooks() {
        this.squares[7][0].setPiece(new Rook(this.playerOneColor, 1));
        this.squares[7][7].setPiece(new Rook(this.playerOneColor, 1));

        this.squares[0][0].setPiece(new Rook(this.playerTwoColor, 2));
        this.squares[0][7].setPiece(new Rook(this.playerTwoColor, 2));
    }

    // Permet de récupérer l'id de l'élément td
    findIdInDOM(e) {
        let id;

        if(e.target.nodeName == 'TD') {
            id = e.target.id;
        }
        else if (e.target.nodeName == 'path'){
            id = e.target.parentNode.parentNode.id;
        }
        else {
            id = e.target.parentNode.id;
        }

        return id;
    }

    // Récupère le square cherché dans this.squares grâce à son id
    findSquare(id) {
        return this.squares.flat().find(square => square.getId() == id);
    }

    // Gère les clics sur l'échiquier
    listenForEvents() {
        $('td').on('click', (e) => {
            let id = this.findIdInDOM(e);
            let square = this.findSquare(id);

            // Si aucune pièce n'est sélectionnée, on la sélectionne
            if(this.selectedSquare == null) {
                if(square.getPiece() != null) {
                    this.selectedSquare = square;
                    let possibleMoves = this.selectedSquare.getPiece().getMoves(this.selectedSquare.getPosition());

                    $('#' + square.id).css('background-color', 'lightgrey');

                    this.possibleMovesToLightgrey(possibleMoves);
                }
            }
            else {
                // Si la pièce cliquée est celle sélectionnée, on la désélectionne
                if(this.selectedSquare.getId() == square.id) {
                    let possibleMoves = this.selectedSquare.getPiece().getMoves(this.selectedSquare.getPosition());

                    $('#' + square.id).css('background-color', 'white');

                    this.possibleMovesToWhite(possibleMoves);

                    this.selectedSquare = null;
                }
                // Si la case cliquée est vide ou que la pièce sélectionnée est ennemie de la pièce cliquée, on déplace
                else if(square.isFree() || (square.getPiece().getPlayer() != this.selectedSquare.getPiece().getPlayer())) {
                    let possibleMoves = this.selectedSquare.getPiece().getMoves(this.selectedSquare.getPosition());

                    for(let i in possibleMoves) {
                        if(square.getPosition().equalsWith(possibleMoves[i])) {
                            this.possibleMovesToWhite(possibleMoves);
                            this.move(this.selectedSquare, square);
                            break;
                        }
                    }
                }
            }
        });
    }

    move(squareFrom, squareTo) {
        squareTo.setPiece(squareFrom.getPiece());
        squareFrom.setPiece(null);
        this.selectedSquare = null;
        $('#board')[0].innerHTML = '';
        this.render();
        this.listenForEvents();
    }

    // Colore les cases possibles de l'échiquier en gris
    possibleMovesToLightgrey(possibleMoves) {
        for(let i in possibleMoves) {
            $('#' + possibleMoves[i].toId()).css('background-color', 'lightgrey');
        }
    }

    // Colore les cases possibles de l'échiquier en blanc
    possibleMovesToWhite(possibleMoves) {
        for(let i in possibleMoves) {
            $('#' + possibleMoves[i].toId()).css('background-color', 'white');
        }
    }

    // Retourne la version html de l'échiquier
    render() {
        let html = '';

        for(let i in this.squares) {
            html += '<tr>';

            for(let j in this.squares[i]) {
                html += this.squares[i][j].render();
            }

            html += '</tr>';
        }

        $('#board').append(html);
    }

}