class Bishop extends Piece {
    constructor(color, player) {
        super(color, player);
        this.view = '<i class="fas fa-chess-bishop fa-2x" style="color: ' + this.color + '"></i>';
    }

    getMoves(position) {
        let positions = [];
        let i;

        if(position.getY() != 7) {
            i = 1;
            while((((position.getX()  + i) <= 7) && ((position.getY() + i) <= 7))) {
                positions.push(new Position(position.getX() + i, position.getY() + i));
                i++;
            }

        }

        if(position.getY() != 0) {
            i = 1;
            while(((position.getX()  - i) >= 0) && ((position.getY() - i) >= 0)) {
                positions.push(new Position(position.getX() - i, position.getY() - i));
                i++;
            }
        }

        if(position.getX() != 7) {
            i = 1;
            while(((position.getX()  + i) <= 7) && ((position.getY() - i) >= 0)) {
                positions.push(new Position(position.getX() + i, position.getY() - i));
                i++;
            }
        }

        if(position.getX() != 0) {
            i = 1;
            while(((position.getX()  - i) >= 0) && ((position.getY() + i) <= 7)) {
                positions.push(new Position(position.getX() - i, position.getY() + i));
                i++;
            }
        }


        return positions;
    }
}