class King extends Piece{
    constructor(color, player) {
        super(color, player);
        this.view = '<i class="fas fa-chess-king fa-2x" style="color: ' + this.color + '"></i>';
    }

    getMoves(position) {
        return [
            new Position(position.getX(), position.getY() - 1),
            new Position(position.getX(), position.getY() + 1),
            new Position(position.getX() - 1, position.getY()),
            new Position(position.getX() + 1, position.getY()),
            new Position(position.getX() - 1, position.getY() - 1),
            new Position(position.getX() + 1, position.getY() + 1),
            new Position(position.getX() + 1, position.getY() - 1),
            new Position(position.getX() - 1, position.getY() + 1)
        ]
    }
}