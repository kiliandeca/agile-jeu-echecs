class Rook extends Piece{
    constructor(color, player) {
        super(color, player);
        this.view = '<i class="fas fa-chess-rook fa-2x" style="color: ' + this.color + '"></i>';
    }

    getMoves(position) {
        let positions = [];

        for(let i = 0; i <= 7; i++) {
            positions.push(new Position(i, position.getY()));
            positions.push(new Position(position.getX(), i));
        }

        return positions;
    }
}