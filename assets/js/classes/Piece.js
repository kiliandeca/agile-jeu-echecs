class Piece {
    color = null;
    player = null;
    view = null;

    constructor(color, player) {
        this.color = color;
        this.player = player;
    }

    getMoves() {

    }

    getColor() {
        return this.color;
    }

    getPlayer() {
        return this.player;
    }

    render() {
        return this.view;
    }
}