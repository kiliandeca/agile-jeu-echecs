class Game {
    chessboard = null;
    isFinished = false;

    constructor() {
        this.chessboard = new Chessboard();
    }
}

function startNewGame() {
    let playerOneColor = document.getElementsByName('player-one-color')[0];
    let playerTwoColor = document.getElementsByName('player-two-color')[0];

    if(playerOneColor.value != 'default' && playerTwoColor.value != 'default' && playerOneColor.value != playerTwoColor.value) {
        sessionStorage.setItem('playerOne', playerOneColor.value);
        sessionStorage.setItem('playerTwo', playerTwoColor.value);

        document.getElementById('new-game').style.display = 'none';

        let game = new Game();
    }
}

$(document).ready(() => {
    if(sessionStorage.getItem('playerOne'))
        sessionStorage.removeItem('playerOne');
    if(sessionStorage.getItem('playerTwo'))
        sessionStorage.removeItem('playerTwo');
});