class Square {
    id = null;
    piece = null;
    position = null;

    constructor(piece, position) {
        this.piece = piece;
        this.position = position;
        this.id = this.position.getX().toString() + this.position.getY().toString();
    }

    getId() {
        return this.id;
    }

    getPiece() {
        return this.piece;
    }

    setPiece(piece) {
        this.piece = piece;
    }

    getPosition() {
        return this.position;
    }

    isFree() {
        return this.piece == null;
    }

    render() {
        if(this.isFree()) {
            return '<td id="' + this.id + '"></td>';
        }
        else {
            return '<td id="' + this.id + '">' + this.piece.render() + '</td>';
        }
    }
}