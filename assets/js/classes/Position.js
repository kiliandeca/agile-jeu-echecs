class Position {
    x = null;
    y = null;
    enabledToEat = true;

    constructor(x, y, enabledToEat = true) {
        this.x = x;
        this.y = y;
        this.enabledToEat = enabledToEat;
    }

    setEnabledToEat(bool) {
        this.enabledToEat = bool;
    }

    getEnabledToEat() {
        return this.enabledToEat;
    }

    getX() {
        return this.x;
    }

    getY() {
        return this.y;
    }

    equalsWith(position) {
        return (this.x == position.x && this.y == position.y);
    }

    toId() {
        return this.x.toString() + this.y.toString();
    }
}